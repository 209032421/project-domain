package com.209032421.Services;

import com.209032421.Domains.Bank;

import java.util.Optional;
import java.util.Set;

public interface BankService {

    public Bank create(Bank bank);

    public Optional<Bank> readByID(int branchCode);

    public Set<Bank> readAll();

    public Bank update(Bank bank);

    public void delete(Bank branchCode);

    public void deleteAll();
}
