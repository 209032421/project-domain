package com.209032421.Controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import projects.ihsaan.Domains.Customer;
import projects.ihsaan.Services.Impl.CustomerServiceImpl;

import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping(path="/customer")
public class CustomerController {

    @Autowired
    private CustomerServiceImpl customerService;

    @GetMapping(path="/all")
    public @ResponseBody
    Set<Customer> getAllCustomer()
    {
        return customerService.readAll();
    }


    @RequestMapping(value="/find/{customerNumber}")
    public @ResponseBody ResponseEntity
    findCustomer(@PathVariable("customerNumber") int customerNumber)
    {
        Optional<Customer> customer = customerService.readByID(customerNumber);
        if(!customer.isPresent())
        {
            return new ResponseEntity("No Customer found with customer number :" + customerNumber, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(customer, HttpStatus.OK);
    }


    @RequestMapping(value="/add/{customerNumber}", method = RequestMethod.POST)
    public ResponseEntity addCustomer(@RequestBody Customer customer){
        if(StringUtils.isEmpty(customer.getCustomerNumber()) || StringUtils.isEmpty(customer.getName()) || StringUtils.isEmpty(customer.getSurname()) || StringUtils.isEmpty(customer.getContactNumber()) || StringUtils.isEmpty(customer.getAccountNumber()))
        {
            return new ResponseEntity("No Empty Fields Allowed", HttpStatus.NO_CONTENT);
        }
        customerService.create(customer);
        return new ResponseEntity(customer, HttpStatus.OK);
    }


    @RequestMapping(value="/update", method = RequestMethod.PUT)
    public ResponseEntity updateCustomer(@RequestBody Customer customer)
    {
        if(StringUtils.isEmpty(customer.getCustomerNumber()) || StringUtils.isEmpty(customer.getName()) || StringUtils.isEmpty(customer.getSurname()) || StringUtils.isEmpty(customer.getContactNumber())
                || StringUtils.isEmpty(customer.getAccountNumber()))
        {
            return new ResponseEntity("No Empty Fields Allowed", HttpStatus.NO_CONTENT);
        }
        customerService.update(customer);
        return new ResponseEntity(customer, HttpStatus.OK);
    }


    @RequestMapping(value="/delete/{customerNumber}", method = RequestMethod.DELETE)
    public void deleteCustomer(@PathVariable Customer customerNumber)
    {
        customerService.delete(customerNumber);
    }


    @RequestMapping(value="/deleteAll", method = RequestMethod.DELETE)
    public void deleteAllCustomer()
    {
        customerService.deleteAll();
    }

}
