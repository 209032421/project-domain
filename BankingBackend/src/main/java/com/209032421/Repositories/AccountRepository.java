package com.209032421.Repositories;

import org.springframework.data.repository.CrudRepository;
import com.209032421.Domains.Account;

public interface AccountRepository extends CrudRepository<Account, Integer> {
}
