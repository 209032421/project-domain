package com.209032421.Repositories;

import org.springframework.data.repository.CrudRepository;
import com.209032421.Domains.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
}
