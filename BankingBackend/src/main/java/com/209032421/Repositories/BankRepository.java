package com.209032421.Repositories;

import org.springframework.data.repository.CrudRepository;
import com.209032421.Domains.Bank;

public interface BankRepository extends CrudRepository<Bank, Integer> {
}
