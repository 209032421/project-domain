package com.209032421.Factories;

import com.209032421.Domains.Account;

public class AccountFactory {
    public static Account getAccount(int accountNumber, double balance, String accountType)
    {
        Account factoryAccount = new Account.Builder()
                .accountNumber(accountNumber)
                .balance(balance)
                .accountType(accountType)
                .build();
        return factoryAccount;
    }
}
