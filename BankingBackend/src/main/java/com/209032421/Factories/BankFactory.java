package com.209032421.Factories;

import com.209032421.Domains.Bank;

public class BankFactory {
    public static Bank getBank(int branchCode, String address, String telephone)
    {
        Bank factoryBank = new Bank.Builder()
                .branchCode(branchCode)
                .address(address)
                .telephone(telephone)
                .build();
        return factoryBank;
    }
}
