package com.209032421.Domains;

import javax.persistence.*;

@Entity
public class Bank {
    @Id
    private int branchCode;
    private String address;
    private String telephone;

    public int getBranchCode() {
        return branchCode;
    }

    public String getAddress() {
        return address;
    }

    public String getTelephone() {
        return telephone;
    }

    private Bank() {
    }

    private Bank(Builder builder) {
        this.branchCode = builder.branchCode;
        this.address = builder.address;
        this.telephone = builder.telephone;
    }

    public static class Builder{
        private int branchCode;
        private String address;
        private String telephone;

        public Builder branchCode(int value) {
            this.branchCode = value;
            return this;
        }

        public Builder telephone(String value) {
            this.telephone = value;
            return this;
        }

        public Builder address(String value) {
            this.address = value;
            return this;
        }

        public Bank build(){
            return new Bank(this);
        }
    }

}
