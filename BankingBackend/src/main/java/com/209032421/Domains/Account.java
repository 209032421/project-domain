package com.209032421.Domains;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Account implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int accountNumber;
    private double balance;
    private String accountType;

    public int getAccountNumber() {
        return accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public String getAccountType() {
        return accountType;
    }

    private Account() {
    }

    public Account(Builder builder){
        this.accountNumber=builder.accountNumber;
        this.balance=builder.balance;
        this.accountType=builder.accountType;
    }


    public static class Builder{
        private int accountNumber;
        private double balance;
        private String accountType;

        public Builder accountNumber(int value){
            this.accountNumber = value;
            return this;
        }

        public Builder balance(double value){
            this.balance = value;
            return this;
        }

        public Builder accountType(String value){
            this.accountType = value;
            return this;
        }

        public Account build(){
            return new Account(this);
        }
    }


}
