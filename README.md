# Project Domain

You are required to design a client (web or mobile) presentation layer for your Spring Boot Domain to show connectivity to your backend with user-friendly error handling.

Submit the client project GITHUB link.
You will be required to present your attempt.
